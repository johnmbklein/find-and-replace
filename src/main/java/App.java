public class App {

  public String findAndReplace(String inputString, String searchTerm, String replaceTerm) {
    System.out.println("search term: " + searchTerm);
    System.out.println("replace term: " + replaceTerm);
    return toLowerCase(inputString.replace(searchTerm.toLowerCase(), replaceTerm.toLowerCase());
  }

}
