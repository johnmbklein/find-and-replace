import org.junit.*;
import static org.junit.Assert.*;

public class AppTest {

  @Test
  public void findAndReplace_wordReplace() {
    App testApp = new App();
    String expected = "This is a blue house.";
    assertEquals(expected, testApp.findAndReplace("This is a red house.", "red", "blue"));
  }

  @Test
  public void findAndReplace_caseInsensivity() {
    App testApp = new App();
    String expected = "This is a blue house.";
    assertEquals(expected, testApp.findAndReplace("This is a Red house.", "red", "blue"));
  }


}
